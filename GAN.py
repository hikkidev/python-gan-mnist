import gc

import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tqdm import tqdm

from Discriminator import build_discriminator
from Generator import build_generator
from utils import plot_images, summarize_diagnostics


class GAN:
    def __init__(self, image_size, num_labels, latent_size=100, learning_rate=2e-4, beta=6e-8):
        self.tag = "gan"
        self.image_size = image_size
        self.channels = 1
        self.latent_size = latent_size
        self.num_labels = num_labels

        input_shape = (self.image_size, self.image_size, self.channels)
        label_shape = (self.num_labels,)
        inputs = Input(shape=input_shape, name='discriminator_input')
        labels = Input(shape=label_shape, name='class_labels')

        self.discriminator = build_discriminator(inputs, labels, image_size)
        optimizer = Adam(learning_rate=learning_rate, beta_1=beta)
        self.discriminator.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        self.discriminator.trainable = False

        input_shape = (latent_size,)
        inputs = Input(shape=input_shape, name='z_input')
        self.generator = build_generator(inputs, labels, image_size)

        optimizer = Adam(learning_rate=learning_rate * 0.5, beta_1=beta * 0.5)
        outputs = self.discriminator([self.generator([inputs, labels]), labels])
        self.gan_model = Model([inputs, labels], outputs, name=self.tag)
        self.gan_model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])

    def train(self, data, epochs, batch_size, save_interval, grid_size, verbose, save_history):
        if verbose:
            self.discriminator.summary()
            self.generator.summary()
            self.gan_model.summary()

        x_train, y_train = data
        noise_input = np.random.uniform(-1.0, 1.0, size=[grid_size, self.latent_size])
        noise_labels = np.eye(self.num_labels)[np.arange(0, grid_size) % self.num_labels]
        train_size = x_train.shape[0]

        print('Начало обучения')
        print(self.tag, "Метки для генерируемых изображений: ", np.argmax(noise_labels, axis=1))
        if save_history:
            history = {'d_loss': [], 'g_loss': [], 'd_acc': [], 'g_acc': []}
        plot_images(generator=self.generator,
                    noise_input=noise_input,
                    noise_labels=noise_labels,
                    tag=self.tag,
                    step=0)
        for epoch in tqdm(range(epochs)):
            rand_pos = np.random.randint(0, train_size, size=batch_size)
            real_images = x_train[rand_pos]
            real_labels = y_train[rand_pos]

            noise = np.random.uniform(-1.0, 1.0, size=[batch_size, self.latent_size])
            fake_labels = np.eye(self.num_labels)[np.random.choice(self.num_labels, batch_size)]
            fake_images = self.generator.predict([noise, fake_labels])
            x = np.concatenate((real_images, fake_images))

            labels = np.concatenate((real_labels, fake_labels))
            y = np.ones([2 * batch_size, 1])
            y[batch_size:, :] = 0.0

            loss, acc = self.discriminator.train_on_batch([x, labels], y)
            log = "\n%d: [D loss: %.8f, acc: %.1f]" % (epoch + 1, loss, acc * 100)
            if save_history:
                history['d_loss'].append(loss)
                history['d_acc'].append(acc * 100)

            noise = np.random.uniform(-1.0, 1.0, size=[batch_size, self.latent_size])
            fake_labels = np.eye(self.num_labels)[np.random.choice(self.num_labels, batch_size)]
            y = np.ones([batch_size, 1])

            loss, acc = self.gan_model.train_on_batch([noise, fake_labels], y)
            log = "%s [G loss: %.8f, acc: %.1f]" % (log, loss, acc * 100)
            if save_history:
                history['g_loss'].append(loss)
                history['g_acc'].append(acc * 100)

            if (epoch + 1) % save_interval == 0:
                if verbose:
                    print(log)
                plot_images(generator=self.generator,
                            noise_input=noise_input,
                            noise_labels=noise_labels,
                            tag=self.tag,
                            step=(epoch + 1))
            # FIXME: Костыль для исправления утечки памяти в tf 2.5.0-rc
            gc.collect()
            tf.keras.backend.clear_session()
        if save_history:
            summarize_diagnostics(history, self.tag)

    def save(self, suffix):
        self.generator.save("%s_%d.h5" % (self.tag, suffix))
