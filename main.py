import argparse
import os

import numpy as np

from my_types import restricted_int, one_digit, positive_float
from utils import plot_images

os.environ["CUDA_VISIBLE_DEVICES"] = "0"

from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import load_model
from GAN import GAN

num_labels = 10
grid_size = 16
latent_size = 100


def load_data():
    (x_train, y_train), (_, _) = mnist.load_data()

    image_size = x_train.shape[1]
    x_train = np.reshape(x_train, [-1, image_size, image_size, 1])
    x_train = x_train.astype('float32') / 255

    y_train = to_categorical(y_train)
    return x_train, y_train, image_size


def train_gan(batch_size, epochs, save_interval, lr, decay, verbose, save_history):
    x_train, y_train, image_size = load_data()

    gan = GAN(image_size=image_size, num_labels=num_labels, latent_size=latent_size, learning_rate=lr, beta=decay)
    gan.train(data=(x_train, y_train),
              epochs=epochs,
              batch_size=batch_size,
              save_interval=save_interval,
              grid_size=grid_size,
              verbose=verbose,
              save_history=save_history)
    gan.save(epochs)


def test_gan(generator, class_label=None):
    tag = "test_outputs"
    noise_input = np.random.uniform(-1.0, 1.0, size=[grid_size, latent_size])
    step = 0
    if class_label is None:
        noise_class = np.eye(num_labels)[np.random.choice(num_labels, grid_size)]
    else:
        noise_class = np.zeros((grid_size, num_labels))
        noise_class[:, class_label] = 1
        step = class_label
    plot_images(generator,
                tag=tag,
                noise_input=noise_input,
                noise_labels=noise_class,
                step=step)
    print('\n')
    print(tag, " Метки для генерируемых изображений: ", np.argmax(noise_class, axis=1))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-v', '--verbose', action='store_true', help='Включить расширенный лог')
    parser.add_argument('--seed', type=restricted_int, help='Задает начальные условия для генератора случайных чисел.',
                        default=42)

    subparser = parser.add_subparsers(dest='command')

    parser_usage = subparser.add_parser('usage', help="Использование готовой модели")
    parser_usage.add_argument('-g', '--generator', help='Название .h5 модели', required=True)
    parser_usage.add_argument('-d', '--digit', type=one_digit, help='Конкретная цифра для генерации')

    parser_train = subparser.add_parser('train', help="Обучение GAN")
    parser_train.add_argument('-e', '--epochs', type=restricted_int, help='Кол-во эпох обучения', default=10_000)
    parser_train.add_argument('-bs', '--batch_size', type=restricted_int, help='Размер мини-выборки', default=64)
    parser_train.add_argument('-lr', '--learning_rate', type=positive_float, help='Скорость обучения', default=2e-4)
    parser_train.add_argument('--beta', type=positive_float, help='Параметр оптимизатора Adam', default=0.5)
    parser_train.add_argument('--interval', type=restricted_int, help='Интервал сохранения изображений при обучении',
                              default=1000)
    parser_train.add_argument('-ph', '--plot_history', action='store_true', help='Нарисовать графики обучения')

    args = parser.parse_args()

    np.random.seed(args.seed)

    if args.command == 'usage':
        model = load_model(args.generator)
        test_gan(model, args.digit)
    elif args.command == 'train':
        print(args)
        train_gan(args.batch_size, args.epochs, args.interval, args.learning_rate, args.beta, args.verbose,
                  args.plot_history)
    else:
        print("Требуется указать идентификатор процесса, проверьте --help.")
