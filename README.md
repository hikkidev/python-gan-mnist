# Обзор
Репозиторий содержит python реализоцию **Generative adversarial network** для генерации цифр основываясь на наборе данных MNIST.

Для установки зависимостей выполните `pip install -r requirements.txt` в папке с проектом.


## Использование

Подробная информация о глобальных параметрах скрипта
1) Перейти в папку с проектом: `cd python-gan-mnist`
2) Выполнить: `python main.py --help`


Инструкция по запуску обучения
1) Перейти в папку с проектом: `cd python-gan-mnist`
2) Подробнее о параметрах скрипта: `python main.py train --help`
3) Выполнить: `python main.py -v train --epochs 10000 –-batch_size 64 `


Инструкция по использованию обученной модели для генерации цифр
1) Перейти в папку с проектом: `cd python-gan-mnist`
2) Подробнее о параметрах скрипта: `python main.py usage --help`
3) Выполнить: `python main.py usage --generator <model_name>.h5 –-digit 8`


## Реализация

Класс реализующий [GAN](./GAN.py)
