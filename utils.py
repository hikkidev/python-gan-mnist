import math
import os
import pickle
import time

import matplotlib.pyplot as plt
import numpy as np


def plot_images(generator, noise_input, noise_labels, tag, step=0):
    os.makedirs(tag, exist_ok=True)
    filename = os.path.join(tag, "%05d.png" % step)
    images = generator.predict([noise_input, noise_labels])
    plt.figure(figsize=(3, 3))
    num_images = images.shape[0]
    image_size = images.shape[1]
    rows = int(math.sqrt(noise_input.shape[0]))
    for i in range(num_images):
        plt.subplot(rows, rows, i + 1)
        image = np.reshape(images[i], [image_size, image_size])
        plt.imshow(image, cmap='gray')
        plt.axis('off')
    plt.savefig(filename)
    plt.close()


def summarize_diagnostics(history, tag):
    plt.figure(figsize=(16, 9))
    plt.subplot(211)
    plt.title('Функция потерь (cross entropy)')
    plt.plot(history['d_loss'], color='blue', label='Дискриминатор')
    plt.plot(history['g_loss'], color='orange', label='Генератор')
    plt.legend(loc='upper right')
    plt.xlabel("Эпоха")
    plt.ylabel("Потери")

    plt.subplot(212)
    plt.title('Точность')
    plt.plot(history['d_acc'], color='blue', label='Дискриминатор')
    plt.plot(history['g_acc'], color='orange', label='Генератор')
    plt.legend(loc='upper right')
    plt.xlabel("Эпоха")
    plt.ylabel("Точность")
    plt.tight_layout()

    stamp = time.strftime("%Y%m%d_%H%M%S")
    filename = stamp + '_plot_' + tag + '.png'
    plt.savefig(filename)
    plt.close()
    print('Кривые обучения сохранены в файл ' + filename)

    f = open(stamp + '_' + tag + '.pkl', "wb")
    pickle.dump(history, f)
    f.close()
