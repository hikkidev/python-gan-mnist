import argparse
from fractions import Fraction


def positive_float(_x):
    try:
        x = float(_x)
        if x <= 0.0:
            raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
    except ValueError:
        try:
            x = Fraction(_x)
            x = x.numerator / x.denominator
            if x <= 0.0:
                raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
        except ValueError:
            raise argparse.ArgumentTypeError("%r не литерал с плавающей точкой" % (_x,))
    return x


def restricted_int(x):
    try:
        x = int(x)
        if x <= 0:
            raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r не целочисленный литерал" % (x,))
    return x


def one_digit(x):
    try:
        x = int(x)
        if x < 0 or x > 9:
            raise argparse.ArgumentTypeError("%s является недопустимым значением" % x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r не целочисленный литерал" % (x,))
    return x
